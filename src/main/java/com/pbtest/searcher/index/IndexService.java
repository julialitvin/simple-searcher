package com.pbtest.searcher.index;

import com.pbtest.searcher.parser.HtmlParser;
import org.apache.commons.validator.routines.UrlValidator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.ExecutorService;

@Service
public class IndexService {

    private static final Logger logger = LoggerFactory.getLogger(IndexService.class);
    private final ExecutorService executorService;
    private volatile int RECURSION_DEPTH;

    private IndexRepository indexRepository;

    public IndexService(ExecutorService executorService,
                        IndexRepository indexRepository) {
        this.executorService = executorService;
        this.indexRepository = indexRepository;
    }


    public void indexUri(String uri, int depth) {
        RECURSION_DEPTH = depth;
        indexDocument(new IndexCommand(uri, 0));
    }


    public DocumentProcessResult indexDocument(IndexCommand indexCommand) {
        DocumentProcessResult documentProcessResult = new DocumentProcessResult()
                .setUri(indexCommand.getUri())
                .setDeepLevel(indexCommand.getDepth())
                .setProcessStatus(ProcessStatus.NOT_ATTEMPTED);

        logger.info("Start processing... | " + indexCommand.getUri());

        if (!UrlValidator.getInstance().isValid(indexCommand.getUri())) {
            logger.error("Input URL is not valid | " + indexCommand.getUri());
            documentProcessResult.setProcessStatus(ProcessStatus.NOT_VALID);
            return documentProcessResult;
        }
        try {
            Document htmlDocument = Jsoup.connect(indexCommand.getUri()).followRedirects(false).get();
            documentProcessResult
                    .setIndexDocument(saveIndexDocument(htmlDocument, indexCommand))
                    .setProcessStatus(ProcessStatus.INDEXED);

            if (documentProcessResult.getDeepLevel() < RECURSION_DEPTH) {
                indexDocumentLinks(documentProcessResult, htmlDocument);
            }
        } catch (IOException e) {
            documentProcessResult.setProcessStatus(ProcessStatus.FAILED);
            logger.error("Error during connection | " + e.getMessage() + " | " + indexCommand.getUri());
            return documentProcessResult;
        }
        documentProcessResult.setProcessStatus(ProcessStatus.COMPLETE);
        logger.info("Finish processing | " + indexCommand.getUri());
        return documentProcessResult;
    }

    public void indexDocumentLinks(DocumentProcessResult parentDocProcResult, Document htmlDocument) {
        HtmlParser.getDocumentLinks(htmlDocument)
                .forEach(link ->
                        executorService.submit(() ->
                                indexDocument(new IndexCommand(link, parentDocProcResult.getDeepLevel() + 1))));
    }


    public IndexDocument saveIndexDocument(Document htmlDocument, IndexCommand indexCommand) {
        logger.info("Start indexation... | " + indexCommand.getUri());
        IndexDocument indexDoc = indexRepository.findById(indexCommand.getUri()).orElse(new IndexDocument());
        indexDoc.setUri(indexCommand.getUri())
                .setTitle(htmlDocument.title())
                .setAuthor(HtmlParser.getAuthor(htmlDocument))
                .setDescription(HtmlParser.getDescription(htmlDocument))
                .setContent(Objects.nonNull(htmlDocument.body()) ? htmlDocument.body().text() : "")
                .setTags(HtmlParser.getTags(htmlDocument));
        IndexDocument indexDocument = indexRepository.save(indexDoc);
        logger.info("Document has been indexed | " + indexCommand.getUri());
        return indexDocument;
    }

}