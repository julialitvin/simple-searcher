package com.pbtest.searcher.index;

public class IndexCommand {
    private String uri;
    private Integer depth;

    public IndexCommand(String uri,
                        Integer depth) {
        this.uri = uri;
        this.depth = depth;
    }

    public String getUri() {
        return uri;
    }

    public IndexCommand setUri(String uri) {
        this.uri = uri;
        return this;
    }

    public Integer getDepth() {
        return depth;
    }

    public IndexCommand setDepth(Integer depth) {
        this.depth = depth;
        return this;
    }
}
