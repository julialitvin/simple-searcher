package com.pbtest.searcher.index;

public enum ProcessStatus {
    COMPLETE, INDEXED, FAILED, NOT_ATTEMPTED, NOT_VALID
}
