package com.pbtest.searcher.index;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Set;

@Document(indexName = "indexdoc", type = "doc")
public class IndexDocument {
    @Id
    @Field(type = FieldType.Text)
    private String uri;
    @Field(type = FieldType.Text)
    private String author;
    @Field(type = FieldType.Text)
    private String description;
    @Field(type = FieldType.Text)
    private String title;
    @Field(type = FieldType.Text)
    private String content;
    @Field(type = FieldType.Text)
    private Set<String> tags;


    public String getUri() {
        return uri;
    }

    public IndexDocument setUri(String uri) {
        this.uri = uri;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public IndexDocument setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public IndexDocument setDescription(String description) {
        this.description = description;
        return this;
    }

    public Set<String> getTags() {
        return tags;
    }

    public IndexDocument setTags(Set<String> tags) {
        this.tags = tags;
        return this;
    }

    public String getContent() {
        return content;
    }

    public IndexDocument setContent(String content) {
        this.content = content;
        return this;
    }

    public String getAuthor() {
        return author;
    }

    public IndexDocument setAuthor(String author) {
        this.author = author;
        return this;
    }
}
