package com.pbtest.searcher.index;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller("/index")
public class IndexController {

    private final IndexService indexService;

    public IndexController(IndexService indexService) {
        this.indexService = indexService;
    }

    @GetMapping
    public ModelAndView openIndexingPage(ModelAndView modelAndView) {
        modelAndView.setViewName("index");
        return modelAndView;
    }

    @PostMapping
    @ExceptionHandler(Exception.class)
    public ModelAndView startIndexing(ModelAndView modelAndView,
                                      @RequestParam(value = "depth", defaultValue = "0") int depth,
                                      @RequestParam(value = "q", defaultValue = "") String uri) {
        indexService.indexUri(uri, depth);
        modelAndView.addObject("depth", depth);
        modelAndView.addObject("uri", uri);
        modelAndView.setViewName("index");
        return modelAndView;
    }
}
