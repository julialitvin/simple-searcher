package com.pbtest.searcher.index;

public class DocumentProcessResult {
    private String uri;
    private ProcessStatus processStatus;
    private IndexDocument indexDocument;
    private int deepLevel;

    public int getDeepLevel() {
        return deepLevel;
    }

    public DocumentProcessResult setDeepLevel(int deepLevel) {
        this.deepLevel = deepLevel;
        return this;
    }

    public String getUri() {
        return uri;
    }

    public DocumentProcessResult setUri(String uri) {
        this.uri = uri;
        return this;
    }

    public ProcessStatus getProcessStatus() {
        return processStatus;
    }

    public DocumentProcessResult setProcessStatus(ProcessStatus processStatus) {
        this.processStatus = processStatus;
        return this;
    }

    public IndexDocument getIndexDocument() {
        return indexDocument;
    }

    public DocumentProcessResult setIndexDocument(IndexDocument indexDocument) {
        this.indexDocument = indexDocument;
        return this;
    }
}
