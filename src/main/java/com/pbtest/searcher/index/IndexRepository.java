package com.pbtest.searcher.index;

import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IndexRepository extends ElasticsearchRepository<IndexDocument, String> {
    @Query("{\"bool\": {\"must\": [{\"query_string\": {\"default_field\": \"uri\", \"query\": \"?0\"}}]}}")
    Optional<IndexDocument> findByUri(String uri);

}
