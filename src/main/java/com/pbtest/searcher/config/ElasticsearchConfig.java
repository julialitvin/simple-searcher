package com.pbtest.searcher.config;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Configuration
@EnableElasticsearchRepositories(basePackages = "com.pbtest.searcher")
@ComponentScan(basePackages = "com.pbtest.searcher")
public class ElasticsearchConfig {

    @Value("${elasticsearch.home}")
    private String ELASTICSEARCH_HOME;

    @Value("${elasticsearch.cluster.name}")
    private String CLUSTER_NAME;

    @Value("${elasticsearch.host}")
    private String HOST;

    @Value("${elasticsearch.port}")
    private Integer PORT;

    @Bean(destroyMethod = "close")
    public Client client() throws UnknownHostException {
        Settings elasticsearchSettings = Settings.builder()
                .put("path.home", ELASTICSEARCH_HOME)
                .put("cluster.name", CLUSTER_NAME).build();
        TransportClient client = new PreBuiltTransportClient(elasticsearchSettings);
        client.addTransportAddress(new TransportAddress(InetAddress.getByName(HOST), PORT));
        return client;
    }

    @Bean
    public ElasticsearchOperations elasticsearchTemplate() throws UnknownHostException {
        return new ElasticsearchTemplate(client());
    }
}
