package com.pbtest.searcher.search;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class SearchController {

    private final SearchService searchService;

    public SearchController(SearchService searchService) {
        this.searchService = searchService;
    }

    @GetMapping("/")
    public ModelAndView showSearchPage(ModelAndView modelAndView) {
        modelAndView.setViewName("search");
        return modelAndView;
    }

    @GetMapping("/search")
    @ExceptionHandler(Exception.class)
    public ModelAndView startSearch(ModelAndView modelAndView,
                                    @RequestParam(value = "q") String query,
                                    @RequestParam(value = "page", defaultValue = "1") Integer pageNumber,
                                    @RequestParam(value = "size", defaultValue = "10") Integer pageSize) {
        Page<SearchResultDto> search = searchService.search(query, pageNumber, pageSize);
        modelAndView.addObject("results", search);

        int totalPages = search.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).boxed().collect(Collectors.toList());
            modelAndView.addObject("pageNumbers", pageNumbers);
        }

        modelAndView.addObject("query", query);
        modelAndView.setViewName("search");
        return modelAndView;
    }
}
