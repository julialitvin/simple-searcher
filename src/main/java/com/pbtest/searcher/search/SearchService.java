package com.pbtest.searcher.search;

import com.pbtest.searcher.index.IndexRepository;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

@Service
public class SearchService {

    private final IndexRepository indexRepository;

    public SearchService(IndexRepository indexRepository) {
        this.indexRepository = indexRepository;
    }

    public Page<SearchResultDto> search(String query, Integer pageNumber, Integer pageSize) {
        return indexRepository.search(new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.multiMatchQuery(query, "content", "title", "description", "tags"))
                .build().getQuery(), getPageable(pageNumber, pageSize))
                .map(SearchResultDto::new);
    }

    private Pageable getPageable(Integer pageNumber, Integer pageSize) {
        int page = pageNumber > 0 ? pageNumber - 1 : 0;
        int size = pageSize > 0 ? pageSize : 10;
        return PageRequest.of(page, size);
    }
}
