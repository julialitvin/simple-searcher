package com.pbtest.searcher.search;

import com.pbtest.searcher.index.IndexDocument;

public class SearchResultDto {
    private String title;
    private String uri;

    public SearchResultDto(IndexDocument document) {
        this.title = document.getTitle();
        this.uri = document.getUri();
    }

    public String getTitle() {
        return title;
    }

    public SearchResultDto setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getUri() {
        return uri;
    }

    public SearchResultDto setUri(String uri) {
        this.uri = uri;
        return this;
    }
}
