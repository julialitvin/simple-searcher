package com.pbtest.searcher.error;

import com.pbtest.searcher.index.IndexService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class AppExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(IndexService.class);

    @ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="Exception occured")
    @ExceptionHandler(Exception.class)
    public ModelAndView handleException(){
        logger.error("Exception handler executed");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("error");
        return modelAndView;
    }
}
