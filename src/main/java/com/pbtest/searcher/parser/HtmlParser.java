package com.pbtest.searcher.parser;

import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class HtmlParser {

    public static Set<String> getDocumentLinks(Document htmlDocument) {
        return htmlDocument.getElementsByAttribute("href").stream()
                .filter(element -> !element.is("script"))
                .map(element -> element.attr("href"))
                .filter(href -> href.startsWith("http"))
                .filter(href -> !href.contains("#"))
                .collect(Collectors.toSet());
    }

    public static String getDescription(Document doc) {
        return doc.getElementsByTag("meta").stream()
                .filter(meta -> meta.attr("name").equals("description"))
                .findFirst()
                .map(meta -> meta.attr("content"))
                .orElse("");
    }

    public static Set<String> getTags(Document doc) {
        return doc.getElementsByTag("meta").stream()
                .filter(meta -> meta.attr("name").equals("keywords"))
                .flatMap(meta -> Arrays.stream(meta.attr("content").split(",")))
                .collect(Collectors.toSet());

    }

    public static String getAuthor(Document doc) {
        return doc.getElementsByTag("meta").stream().
                filter(meta -> meta.attr("name").equals("author"))
                .findFirst().map(meta -> meta.attr("content"))
                .orElse("");
    }

}
