package com.pbtest.searcher.index;

import com.pbtest.searcher.parser.HtmlParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Disabled;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.ExecutorService;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class IndexServiceTest {

    @InjectMocks
    private IndexService indexService;

    @Mock
    private IndexRepository indexRepository;

    @Mock
    private ExecutorService executorService;

    @Mock
    private HtmlParser htmlParser;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    @Disabled
    //TODO need to finish
    public void testSaveIndexDocument() {
        String html = "<!DOCTYPE HTML>" +
                "<html lang=\"en\">" +
                "<head>" +
                "    <title>Some test title<title/>" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">" +
                "    <meta  name=\"description\" content=\"some test description\">" +
                "    <meta name=\"description\" content=\"test text\">" +
                "</head>" +
                "<body>" +
                "<p>Test text</p>" +
                "</body>" +
                "</html>";
        Document document = Jsoup.parse(html);
        Mockito.when(indexService.saveIndexDocument(document, new IndexCommand("http://test.uri", 0)))
                .thenReturn(new IndexDocument());
    }

}
