package com.pbtest.searcher.parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.Set;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class HtmlParserTest {

    @Test
    public void shouldReturnTrue_whenParseHtmlDocument_onDescription() {
        String html = "<!DOCTYPE HTML>" +
                "<html lang=\"en\">" +
                "<head>" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">" +
                "    <meta  name=\"description\" content=\"some test description\">" +
                "    <meta name=\"description\" content=\"test text\">" +
                "</head>" +
                "<body>" +
                "<p>Test text</p>" +
                "</body>" +
                "</html>";
        Document document = Jsoup.parse(html);
        String description = HtmlParser.getDescription(document);

        Assertions.assertEquals("some test description", description);
        Assertions.assertNotEquals("test text", description);
    }

    @Test
    public void shouldReturnTrue_whenParseHtmlDocument_onAuthor() {
        String html = "<!DOCTYPE HTML>" +
                "<html lang=\"en\">" +
                "<head>" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">" +
                "    <meta name=\"author\" content=\"some test author\">" +
                "    <meta name=\"author\" content=\"test text\">" +
                "</head>" +
                "<body>" +
                "<p>Test text</p>" +
                "</body>" +
                "</html>";
        Document document = Jsoup.parse(html);
        String author = HtmlParser.getAuthor(document);

        Assertions.assertEquals("some test author", author);
        Assertions.assertNotEquals("test text", author);
    }

    @Test
    public void shouldReturnTrue_whenParseHtmlDocument_OnKeywords() {
        String html = "<!DOCTYPE HTML>" +
                "<html lang=\"en\">" +
                "<head>" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">" +
                "    <meta name=\"keywords\" content=\"some,test,keywords\">" +
                "</head>" +
                "<body>" +
                "<p>Test text</p>" +
                "</body>" +
                "</html>";
        Document document = Jsoup.parse(html);
        Set<String> description = HtmlParser.getTags(document);

        Assertions.assertTrue(description.containsAll(Arrays.asList("some", "test", "keywords")));
    }

    @Test
    public void shouldReturnTrue_whenParseHtmlDocument_onLinks() {
        String html = "<!DOCTYPE HTML>" +
                "<html lang=\"en\">" +
                "<head>" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">" +
                "    <meta name=\"keywords\" content=\"some,test,keywords\">" +
                "</head>" +
                "<body>" +
                "<p href=\"hhhhttph://example0.com\">Test text</p>" +
                "<p href=\"httph://example1.com\">Test text</p>" +
                "<p href=\"httph://example2.com\">Test text</p>" +
                "<p href=\"httph://example3.com?q=2#param\">Test text</p>" +
                "</body>" +
                "</html>";
        Document document = Jsoup.parse(html);
        Set<String> links = HtmlParser.getDocumentLinks(document);
        Assertions.assertTrue(links.containsAll(Arrays.asList("httph://example1.com", "httph://example2.com")));
        Assertions.assertFalse(links.contains("hhhhttph://example0.com"));
        Assertions.assertFalse(links.contains("httph://example3.com?q=2#param"));
        Assertions.assertEquals(2, links.size());
    }
}
