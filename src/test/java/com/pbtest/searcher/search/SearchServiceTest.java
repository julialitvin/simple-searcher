package com.pbtest.searcher.search;

import com.pbtest.searcher.index.IndexCommand;
import com.pbtest.searcher.index.IndexRepository;
import com.pbtest.searcher.index.IndexService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Disabled;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;

import java.util.concurrent.ExecutorService;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class SearchServiceTest {

    @InjectMocks
    private IndexService indexService;

    @InjectMocks
    private SearchService searchService;

    @Mock
    private IndexRepository indexRepository;

    @Mock
    private ExecutorService executorService;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    @Disabled
    //TODO need to fix NPE and finish
    public void shouldReturnTrue_WhenSearch(){
        String html = "<!DOCTYPE HTML>" +
                "<html lang=\"en\">" +
                "<head>" +
                "    <title>Some test title<title/>" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">" +
                "    <meta  name=\"description\" content=\"some test description\">" +
                "    <meta name=\"description\" content=\"test text\">" +
                "</head>" +
                "<body>" +
                "<p>Test text</p>" +
                "</body>" +
                "</html>";
        Document document = Jsoup.parse(html);
        indexService.saveIndexDocument(document, new IndexCommand(Mockito.anyString(), 0));
        Mockito.when(searchService.search("text", 0, 10))
                .thenReturn(new PageImpl<>(Mockito.anyList()));
    }}
