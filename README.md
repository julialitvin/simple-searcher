# Search application

Get start with Docker
```
docker run --name searcher -d -p 9200:9200 -p 9300:9300 -e cluster.name=elasticsearch -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:6.4.3
```